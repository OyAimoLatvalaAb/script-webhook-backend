const packageErrorMsg = (msg) => JSON.stringify({
    event: 'status',
    status: 'error',
    message: msg,
});

export default packageErrorMsg;
