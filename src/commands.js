import logger from './logger';
import realtimeShell from './realtimeShell';

const webshopExporterPath = '/opt/webshop_exporter';

const normalProductSyncCommand = {
    bin: 'sudo',
    args: [`${webshopExporterPath}/runme.sh`],
};
const nukeProductSyncCommand = {
    bin: 'sudo',
    args: [`${webshopExporterPath}/runme_nuke.sh`],
};

const commands = {
    'trigger': (params, ws) => {
        let doNuke = false;
        if(Object.prototype.hasOwnProperty.call(params, 'nuke') && params.nuke === true) {
            doNuke = true;
        }
        logger.info(`Triggering product sync.${doNuke ? ' WITH NUKE!!!' : ''}`);
        let shellCmd;
        if(doNuke) {
            shellCmd = nukeProductSyncCommand;
        } else {
            shellCmd = normalProductSyncCommand;
        }
        logger.info(`Executing "${shellCmd.bin} ${shellCmd.args.join(' ')}"`);
        realtimeShell(shellCmd, ws);
        return true;
    },
};

export default commands;
