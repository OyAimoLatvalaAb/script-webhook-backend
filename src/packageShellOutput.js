const packageShellOutput = (buffer, mode) => JSON.stringify({
    event: 'output',
    output: buffer,
    mode: mode,
});

export default packageShellOutput;
