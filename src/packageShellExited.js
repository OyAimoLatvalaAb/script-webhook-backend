const packageShellExited = (code) => JSON.stringify({
    event: 'shellExit',
    code: code,
});

export default packageShellExited;
