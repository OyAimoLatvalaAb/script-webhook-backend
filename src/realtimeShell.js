import { spawn } from 'child_process';
import packageShellOutput from './packageShellOutput';
import packageShellExited from './packageShellExited';
import packageShellError from './packageShellError';
import logger from './logger';

const realtimeShell = (cmd, ws) => {
    logger.debug('Spawning shell');
    const shell = spawn(cmd.bin, cmd.args);
    logger.debug('Setting shell character encoding');
    shell.stdout.setEncoding('utf8');
    shell.stderr.setEncoding('utf8');
    logger.debug('Registering functions');
    shell.stdout.on('data', (chunk) => {
        logger.debug(`Got STDOUT data: ${chunk}`);
        const response = packageShellOutput(chunk, 'STDOUT');
        ws.send(response);
    });
    shell.stderr.on('data', (chunk) => {
        logger.debug(`Got STDERR data: ${chunk}`);
        const response = packageShellOutput(chunk, 'STDERR');
        ws.send(response);
    });
    shell.on('close', (code) => {
        logger.debug('Shell exited with code %d', code);
        ws.send(packageShellExited(code));
    });
    shell.on('error', (err) => {
        logger.error('Error executing child process: %j', err);
        ws.send(packageShellError(err));
    });
};

export default realtimeShell;
