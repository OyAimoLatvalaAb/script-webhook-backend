import 'dotenv/config';

import logger from './logger';

import WebSocket from 'ws';
import messageHandler from './messageHandler';

const main = (argc, argv) => { // eslint-disable-line
    if(process.env.AUTH_TOKEN.trim() === '') {
        logger.error('Please set env variable AUTH_TOKEN');
        process.exit(1);
    }
    const wss = new WebSocket.Server({
        port: process.env.HTTP_PORT,
        perMessageDeflate: {
            zlibDeflateOptions: {
                // See zlib defaults.
                chunkSize: 1024,
                memLevel: 7,
                level: 3,
            },
            zlibInflateOptions: {
                chunkSize: 10 * 1024,
            },
            // Other options settable:
            clientNoContextTakeover: true, // Defaults to negotiated value.
            serverNoContextTakeover: true, // Defaults to negotiated value.
            serverMaxWindowBits: 10, // Defaults to negotiated value.
            // Below options specified as default values.
            concurrencyLimit: 10, // Limits zlib concurrency for perf.
            threshold: 1024, // Size (in bytes) below which messages
            // should not be compressed.
        },
    });
    wss.on('connection', (ws, request) => { // eslint-disable-line
        logger.info('Got new connection.');
        ws.on('message', (message) => messageHandler(ws, message));
    });
    logger.info('WSS started.');
};

main(process.argc, process.argv);
