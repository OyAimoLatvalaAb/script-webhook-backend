import logger from './logger';
import commands from './commands';
import packageErrorMsg from './packageErrorMsg';


const validMessageData = (data) => {
    if(Object.prototype.hasOwnProperty.call(data, 'command') &&
        Object.prototype.hasOwnProperty.call(data, 'params') &&
        typeof data.command === 'string' &&
        typeof data.params === 'object') {
        return true;
    }
    return false;
};

const handler = (ws, message) => {
    let data;
    try {
        data = JSON.parse(message);
        logger.debug('%j', data);
    } catch(e) {
        logger.error('Client sent invalid JSON data: "%s"', message);
        ws.send(packageErrorMsg('Invalid JSON input'));
        return false;
    }
    if(validMessageData(data)) {
        const authToken = data.params.authToken+'';
        if(process.env.AUTH_TOKEN !== authToken.trim()) {
            logger.error('Unauthorized trigger attempt.');
            ws.send(packageErrorMsg('Unauthorized'));
            return true;
        }
        else if(!Object.prototype.hasOwnProperty.call(commands, data.command)) {
            const errorMsg = `No such command: ${data.command}`;
            ws.send(packageErrorMsg(errorMsg));
            logger.warn(errorMsg);
        } else if(!commands[data.command](data.params, ws)) {
            logger.error('Error executing %s with params %j', data.command, data.params);
        }
    } else {
        const errorMsg = 'Invalid message. Please make sure all required keys are present and in correct format.';
        logger.warn('Client sent invalid message: %j', data);
        ws.send(packageErrorMsg(errorMsg));
    }
};

export default handler;
